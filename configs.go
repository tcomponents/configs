package configs

import (
	"errors"
	"fmt"
	"os"

	"github.com/hashicorp/consul/api"
)

var (
	// ErrKeyNotExist for invalid key provided
	ErrKeyNotExist = errors.New("the key does not exist")
	// ErrNotInitialized for uninitialized
	ErrNotInitialized = errors.New("the client does not be initialized, call With() first")
	// ErrEmptyToken missing token
	ErrEmptyToken = errors.New("missing a valid token for accessing")
	// ErrNotReady missing some params
	ErrNotReady = errors.New("missing some params")
)

var (
	option = Option{
		DataCenter: "dc-main",
		Env:        Development,
		Endpoints: Endpoints{
			Development: "https://configs.trust.university",
			Production:  "10.3.254.76:8500",
		},
	}
	c *api.Client
)

type Endpoints struct {
	Development string
	Production  string
}

// Option an option set
type Option struct {
	Token      string
	DataCenter string
	Env        Environment
	Endpoints  Endpoints
}

// Ready determines whether the client can be made or not
func (o Option) Ready() bool {
	if o.Token == "" {
		return false
	}
	if o.DataCenter == "" {
		return false
	}
	return true
}

func init() {
	if token := os.Getenv("CONFIGS_TOKEN"); token != "" {
		option.Token = token
	}
	// try to make a client
	// error is ignored since the With() can reconnect later
	_ = client()
}

// Environment defines different envs
type Environment string

// String to string
func (e Environment) String() string {
	return string(e)
}

func (e Environment) Endpoint() string {
	if e == Production {
		return option.Endpoints.Production
	}
	return option.Endpoints.Development
}

const (
	// Development for developing
	Development Environment = "development"
	// Production for producting
	Production Environment = "production"
)

// ENV will return what current environment does the system use
func ENV() Environment {
	return option.Env
}

// With will customize your client
func With(opts ...Option) error {
	for _, opt := range opts {
		if opt.Token != "" {
			option.Token = opt.Token
		}
		if opt.DataCenter != "" {
			option.DataCenter = opt.DataCenter
		}
		if opt.Endpoints.Development != "" {
			option.Endpoints.Development = opt.Endpoints.Development
		}
		if opt.Endpoints.Production != "" {
			option.Endpoints.Production = opt.Endpoints.Production
		}
		if opt.Env == Production {
			option.Env = Production
		}
	}
	// recreate client
	return client()
}

// Set will push a config pair to consul
func Set(key, val string) error {
	if c == nil {
		return ErrNotInitialized
	}
	_, err := c.KV().Put(
		&api.KVPair{
			Key:   fmt.Sprintf("%s/%s", option.Env.String(), key),
			Value: []byte(val),
		}, nil,
	)
	if err != nil {
		return err
	}
	return nil
}

// Get returns the value of the key from consul
func Get(key string) (string, error) {
	if c == nil {
		return "", ErrNotInitialized
	}
	data, _, err := c.KV().Get(
		fmt.Sprintf("%s/%s", option.Env.String(), key),
		nil,
	)
	if err != nil {
		return "", err
	}
	if data == nil {
		return "", ErrKeyNotExist
	}
	return string(data.Value), nil
}

func client() error {
	if !option.Ready() {
		return ErrNotReady
	}
	cfg := api.DefaultConfig()
	cfg.Datacenter = option.DataCenter
	cfg.Token = option.Token
	cfg.Address = option.Env.Endpoint()
	client, err := api.NewClient(cfg)
	if err != nil {
		return err
	}
	c = client
	return nil
}
